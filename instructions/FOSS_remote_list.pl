@ENTRIES = (
   "gh-zm" => { 'url-prefix' => "https://gitlab.raptorengineering.com/raptor-engineering-public/zimbra", },
   "gh-ks" => { 'url-prefix' => "https://github.com/kohlschutter", },
   "gh-ms" => { 'url-prefix' => "https://gitlab.raptorengineering.com/raptor-engineering-public/zimbra", },
   "zm-ow"  => { 'url-prefix' => "https://gitlab.raptorengineering.com/raptor-engineering-public/zimbra", },
);
